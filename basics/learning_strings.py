"""
literals
"""
# String literal
'hello world'
# numeric literal
15
-5
0

# list literal
[]
[1, 5, 8, 99]
['this', 'words']

"""
Strings
"""

'Hello World'
rio = 'Hello World'

print('Hello World')
print(rio)

"""
variable    operator    equation=>literal
alex        =           'My favorite phrase is: ' + rio
alex        =           'My favorite phrase is: ' + 'Hello World'
alex        =           'My favorite phrase is: Hello World'
"""
alex = 'My favorite phrase is: ' + rio
print(alex)

a = 'dog'
b = 'cat'
c = ' vs '
final = a + c + b
"""
equation=>literal
print(a + c + b)
print('dog' + c + b)
print('dog' + ' vs ' + b)
print('dog' + ' vs ' + 'cat')
print('dog vs cat')
"""
print(a + c + b)
print(final)
print(a + ' vs ' + b)
