"""
define command  function name   parameters          terminator
def             the_father      (child1, child2)    :

return
'I am the father of ' + child1 + ' and ' + child2
"""
def the_father(child1, child2):
    print('The name of my first child is: ' + child1)
    print('The name of favorite child is: ' + child2)
    print('However, I love them both the same.')
    return 'I am the father of ' + child1 + ' and ' + child2


"""
equation=>literal
the_father('Vicky', 'Morty')
'I am the father of Vicky and Morty'
"""
the_father('Vicky', 'Morty')

"""
variable    operator    equation=>literal
truthful    =           the_father('Alex', 'Julian')
truthful    =           'I am the father of Alex and Julian'
"""
truthful = the_father('Alex', 'Julian')
print(truthful)
"""
equation=>literal
print(the_father('Mike', 'Tony'))
print('I am the father of Mike and Tony')
None
"""
print(the_father('Mike', 'Tony'))

"""
variable    operator    equation=>literal
example     =           print('hahaha')
example     =           None
"""
example = print('hahaha')
print(example)

"""
define command  function name   parameters  terminator
def             bill            (status)    :

return
None
"""
def bill(status):
    print('I am the best, check out this status: ' + status)


bill('currently winning at programming')
roxi = bill('currently chasing the dog :(')
print(roxi)

"""
define command  function name   parameters  terminator
def             final_function  (jake, tim) :
"""
def final_function(jake, tim):
    print('I never follow the rules!')
    return None


saved_return = final_function('money', 'power')
print(saved_return)
