class College(object):
    def __init__(self, name, students=None, majors=None, minors=None):
        self.name = name
        self.students = []
        self.majors = []
        self.minors = []

        if students is not None:
            self.students = students
        if majors is not None:
            self.majors = majors
        if minors is not None:
            self.minors = minors

    """
        The current number of students.
        
        :returns: The number of students in the college.
        :rtype: int
    """
    def get_number_of_students(self):
        pass

    """
        Gets the students with the provided major.
        
        :param major: The major used to search. 
        :returns: A list of all students in the provided major.
        :rtype: list
    """
    def get_students_by_major(self, major):
        pass

    """
        Gets the students with a GPA greater than or equal to the provided GPA. 
        
        :param gpa: The minimum required GPA. 
        :returns: A list of all students at or above the GPA provided.
        :rtype: list
    """
    def get_students_by_gpa(self, gpa):
        pass

    """
        Gets the students with the provided minor.

        :param minor: The major used to search. 
        :returns: A list of all students in the provided minor.
        :rtype: list
    """

    def get_students_by_minor(self, minor):
        pass

    """
        A dump of all student specific data including the students name. 
        
        :returns: The output string containing each student's name (in 'last, first' format) 
        and the student specific information, (major, minor (if applicable), GPA) in order by 
        student's last name. One student per line.
        :rtype: string
        
        Example:
        Anderson, Nelson - Software - 3.37
        Welsh, Jerry - Accounting, Art - 2.4
    """
    def dump_student_details(self):
        pass

    def __str__(self):
        return 'College( ' \
               + 'name="' + self.name \
               + '", students=[\n' + ',\n'.join(student.__str__() for student in self.students) + '\n]' \
               + ', majors=[\n' + ',\n'.join(major for major in self.majors) + '\n]' \
               + ', minors=[\n' + ',\n'.join(minor for minor in self.minors) + '\n]' \
               + ' )'
# End
