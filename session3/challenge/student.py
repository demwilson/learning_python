class Student(object):
    def __init__(self, first_name, last_name, sex, age, student=True, major='undecided', minor=None, gpa=0, notes=None):

        self.first_name = first_name
        self.last_name = last_name
        self.sex = sex
        self.student = bool(student)
        self.major = major
        self.minor = minor
        self.gpa = gpa

        # Validation
        try:
            self.age = int(age)
        except ValueError:
            raise ValueError('Expected age to be type int but got {} instead.\nActual value: "{}"'.format(type(age), age))
        if notes is not None:
            self.notes = str(notes)
        else:
            self.notes = notes

    """
        Get the student's name.
        :returns: The students name in this format: 'last, first' 
    """
    def get_name(self):
        pass

    """
        :returns: A string containing:
         1. The student's name in the 'last, first' format followed by,
         2. the student specific information: major, minor (if the student has one), and gpa
    """
    def get_student_details(self):
        pass

    def __str__(self):
        return 'Student( ' \
               + 'first_name="' + self.first_name \
               + '", last_name="' + self.last_name \
               + '", age=' + str(self.age) \
               + ', sex="' + str(self.sex) \
               + '", student=' + str(self.student) \
               + ', major="' + str(self.major) \
               + '", minor="' + str(self.minor) \
               + '", gpa=' + str(self.gpa) \
               + ', notes="' + str(self.notes) \
               + '" )'
