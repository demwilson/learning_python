"""
    Session 3 Challenge: College Students

    We have a college full of students. Well... we want a college full of students.
    Complete the functions with a pass in them across the three major files and make sure all
    the prints at the bottom work.

    Files you will probably touch: college.py, start.py, student.py

    If you need any help or guidance, feel free to message me on discord, messenger,
    gmail, or just give me a call.

    I will upload a completed solution into a folder called solution the day before our
    next session.
"""

from session3.challenge.college import College

# Here are the predefined majors and minors
predefined_majors = ['Computer Science', 'Communications', 'Political Science',
                     'Business', 'Economics', 'English', 'Psychology', 'Nursing']
predefined_minors = ['Business', 'Communication', 'Spanish', 'Sociology', 'Religious Studies',
                     'Psychology', 'History', 'Family Studies']

"""
    Create a function that will create a number of random students with varying attributes.
    You must AT LEAST randomize the major, minor, and gpa of each student created.
    How you do that is up to you.
    
    :param number_of_students: The number of students to create.
    :returns: List of created students.
"""
def create_students(number_of_students):
    pass


my_students = create_students(10)

my_college = College('Williamson College', my_students, majors=predefined_majors, minors=predefined_minors)

print('There are currently {} students enrolled at {}.'.format(my_college.get_number_of_students(), my_college.name))

print('Here are all the students in alphabetical order:')
print(my_college.dump_student_details())

print('Here are the names of all students with a Nursing major:')
students_by_major = my_college.get_students_by_major('Nursing')
print('\n'.join(student.get_name() for student in students_by_major))

print('Here are the names of all students with a Spanish minor:')
students_by_minor = my_college.get_students_by_minor('Spanish')
print('\n'.join(student.get_name() for student in students_by_minor))

print('Here are the students with a passing GPA:')
students_by_gpa = my_college.get_students_by_gpa(2)
print('\n'.join(student.get_name() + ' - ' + str(student.gpa) for student in students_by_gpa))
