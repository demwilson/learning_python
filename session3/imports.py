# import allows the whole library to be used in the application
import math
# from import allows the specified functions to be used in the application
from random import randint

# Creating my variables

# raise a number to the power of another number
powered = math.pow(2, 3)
# round up a number with .ceil
rounded = math.ceil(4.657)
# A random integer between 40 and 300
my_number = randint(40, 300)

# Showing the results
print('2^3: {}'.format(powered))
print('rounded up: {}'.format(rounded))
print('my random number: {}'.format(my_number))
