def lol(text):
    return "~{}~".format(text)


items = ['donald', lol, 7, lol('oops'), [], 1455]
for item in items:
    print(item)

items_as_string = ', '.join(lol(item) for item in items)
print(items_as_string)

joiner = ', '
built_string = ''
for item in items:
    built_string = built_string + str(item) + joiner

print(built_string)

##### LISTS

