# Loops are unique in python
# You can do some really interesting stuff by combining string functions with loop outputs
names = ['Will', 'Michael', 'Tony', 'Alexander']
# You can print each item in the list
short_names = ', '.join(name for name in names)
print(short_names)

# This one requires some extra work to make sure it works for all items in the list
my_list = ['Hello', 246, False, []]
converted_list = '["' + '", "'.join(str(item) for item in my_list) + '"]'
print(converted_list)
