import math
from random import randint

# 5^4 or 5*5*5*5
donkey = math.pow(2, 4)
# 1 - 6
a = 1
b = 2
die_roll = randint(1, 6)
die_roll2 = randint(1, 6)

# line 11 and 12 are the same.
print('2^4 = {}'.format(donkey))
print('2^4 = ' + str(donkey))
print(str(die_roll))
print(str(die_roll2))
print('{}, {}'.format(die_roll, die_roll2))
print('{}, {}'.format(randint, ['56', 98, True], 'hello'))

###########
