# an enumeration is a value type defined by a set of named constants
# NOTE: the constants are usually integers, but not always
from enum import IntEnum, Enum


# Example of integer based enums
class Seasons(IntEnum):
    WINTER = 0
    SPRING = 1
    SUMMER = 2
    FALL = 3


# Example of string based enums
class Status(Enum):
    PENDING = 'pending'
    ACTIVE = 'active'
    RETIRED = 'retired'
    REMOVED = 'deleted'


# assigning enums to variables
selected_season = Seasons.SPRING
selected_status = Status.RETIRED

# IntEnum and Enum act differently when printed

# IntEnum returns the number
print('Selected Season: {}\n'
      '    name: {}\n'
      '    value: {}'.format(
        selected_season,
        selected_season.name,
        selected_season.value)
)

# Enum returns the entire enumeration item (Status.RETIRED)
print('Selected Status: {}\n'
      '    name: {}\n'
      '    value: {}'.format(
        selected_status,
        selected_status.name,
        selected_status.value)
)

# debugging with enums
