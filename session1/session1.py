x = 7
y = 4
z = x + y
print(z)


# basic function
def add(param1, param2):
    return param1 + param2


print(add(11, 22))

# lists
my_list = [1, 3, 5, 7]
my_list.append(9)

# Functions with loops

def add_items(def_list):
    total = 0
    for item in def_list:
        total = total + item
    return total


print(add_items(my_list))

# Dictionaries

dic = {
    'mike': 'laughing',
    'tony': 'thinks he\'s better than me',
    'will': 'he\'ll catch up eventually',
}


def what_are_we_doing(def_dic):
    print('What are they doing?')
    for person in def_dic:
        print(
            '{} is {}!'.format(
                person,
                def_dic[person]
            )
        )


what_are_we_doing(dic)

example = {
    "item1": {
        "item2": {
            "item3": []
        }
    }
}
