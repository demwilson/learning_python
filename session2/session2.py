# name = 'Will'
# age = 16
# sex = True
# student = True
#
# name2 = 'Juan'
# age2 = 9
# sex2 = False
# student2 = True
#
# name3 = "Mike"
# age3 = 7
# sex3 = False
# student3 = True

# Name
# Age
# sex


class Person(object):
    def __init__(self, first_name, last_name, sex, student=False, age=18, notes=None):
        self.first_name = first_name
        self.last_name = last_name
        self.sex = sex
        self.student = bool(student)
        # Validation
        try:
            self.age = int(age)
        except ValueError:
            self.age = 18
        if notes is not None:
            self.notes = str(notes)
        else:
            self.notes = notes

    def get_name(self):
        return '{}, {}'.format(
            self.last_name,
            self.first_name
        )

    def __str__(self):
        return 'Person( ' \
               + 'first_name="' + self.first_name \
               + '", last_name="' + self.last_name \
               + '", age=' + str(self.age) \
               + ', sex="' + str(self.sex) \
               + '", student=' + str(self.student) \
               + ', notes="' + str(self.notes) \
               + '" )'


person = Person('Will', 'Wilson', 'male', 0, 'notold')
person2 = Person('Mike', 'Marquez', 'male', True, 33)
person3 = Person('Tony', 'Davila', 'male', notes='my notes on tony')

print('The best students:')
print(person.get_name())
print(person2.get_name())
print(person3.get_name())
print()
print(person)
print(person2)
print(person3)
# print(will.__str__())