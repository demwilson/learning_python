# int
# float
# str
# round(x, decimal_places)
"""
~~~~~~~Example 1~~~~~~~
i = input('Enter Number: ')
value = round(float(i), 2)

print('This is your input: ' + i)
print('This is your number: ' + str(value))
"""

force_loop = True
while force_loop:
    i = input('Enter an integer: ')
    try:
        value = int(i)
        force_loop = False
    except ValueError:
        print('You must enter an integer, nothing else! YOU GET NOTHING!\n')

print('Your totally legitimate number: ' + str(value))
