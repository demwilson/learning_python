"""
Challenge 4:
    Create a RPS game where the user chooses an option, and the computer selects a random option from RPS.

 DETAILS:
 Rock, Paper, Scissors (RPS)
 2 players
 Rules:
 1. Each player chooses one option from RPS
 2. The options selected are revealed and the game rules are checked.
    a. rock beats scissors
    b. scissors beats paper
    c. paper beats rock
    d. otherwise tie
 3. Winner is declared.
"""
