def get_age_greeting(age):
    response = 'You look well!'
    if age < 18:
        response = 'Aren\'t you a little young to be messing with my program?'
    elif age > 70:
        response = 'OH, SORRY, I FORGOT ABOUT YOUR HEARING! HELLO, HOW ARE YOU FEELING?'
    return response


name = input("Enter your name: ")
age = int(input("Enter your age: "))

greeting = get_age_greeting(age)
print_string = '\nHello, ' + name + '! ' + greeting

print(print_string)
