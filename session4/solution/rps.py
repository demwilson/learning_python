"""
Challenge 4:
    Create a RPS game where the user chooses an option, and the computer selects a random option from RPS.

 DETAILS:
 Rock, Paper, Scissors (RPS)
 2 players
 Rules:
 1. Each player chooses one option from RPS
 2. The options selected are revealed and the game rules are checked.
    a. rock beats scissors
    b. scissors beats paper
    c. paper beats rock
    d. otherwise tie
 3. Winner is declared.
"""

from random import randint

ROCK = 1
PAPER = 2
SCISSORS = 3

# First position is skipped because the player will
selection_map = ['rock', 'paper', 'scissors']

user_selection = None
computer_selection = None

while user_selection is None:
    selection = None
    computer_selection = randint(0, 2)
    user_input = input('Choose 1)rock, 2)paper, or 3)scissors: ')

    try:
        selection = int(user_input)
    except:
        pass

    if selection is not None and ROCK <= selection <= SCISSORS:
        user_selection = selection - 1
    else:
        print('You must select a number between 1 and 3.\n')

# Game Rules
winner = None
if user_selection == ROCK:
    if computer_selection == SCISSORS:
        winner = 'player'
    elif computer_selection == PAPER:
        winner = 'computer'
elif user_selection == PAPER:
    if computer_selection == ROCK:
        winner = 'player'
    elif computer_selection == SCISSORS:
        winner = 'computer'
elif user_selection == SCISSORS:
    if computer_selection == PAPER:
        winner = 'player'
    elif computer_selection == ROCK:
        winner = 'computer'

if winner is None:
    print('\nBoth the player and the computer chose ' + selection_map[user_selection] + ', so it\'s a tie!')
else:
    print('\nThe player chose ' + selection_map[user_selection] + '.\nThe computer chose ' + selection_map[computer_selection] + '.\n\nThe ' + winner + ' wins!')
